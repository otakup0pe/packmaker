FROM python:3-slim

ARG PACKMAKER_VERSION

ADD dist/packmaker-*.tar.gz /tmp/
RUN export PACKMAKER_VERSION="${PACKMAKER_VERSION:-localdev}"; \
    pip3 install requests pyyaml \
    && cd /tmp/packmaker-* && python3 setup.py install \
    && rm -rf /tmp/packmaker-*

CMD ["bash"]
