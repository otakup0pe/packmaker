# PACKMAKER

Packmaker is a command line tool used to build Modded Minecraft modpacks on
Linux systems. It can take a yaml-based description of a pack and generate
a package suitable for use in the following formats:

* **Curseforge** - A curseforge package suitable for uploading directly to the
  curseforge.com webiste, or importing into a curseforge compatible minecraft
  launcher like MultiMC or the Twitch Client.

* **Routhio** - A package format used by the minecraft.routh.io community and the
  routh.io minecraft launcher.

* **Local client** - A local minecraft client installation, suitable for standalone
  games.  Packmaker can even launch your modded minecraft installation in
  offline mode itself. Great for testing your modpack during development.

* **Server** - A minecraft server installation, suitable for use by clients to
  connect with and play together.

For more information, see the official documentation site:

   https://packmaker.readthedocs.io
